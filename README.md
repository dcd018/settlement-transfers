## Example Laravel API

This is a Docker Compose stack based loosely off of [docker-lnpp](https://github.com/dcd018/docker-lnpp "docker-lnpp") supporting both Unix-like and Windows file-systems with bind mount and named volumes respectively.

### Usage

#### Development (Unix only)
```
git clone git@gitlab.com:dcd018/settlement-transfers.git
cd settlement-transfers
./export_env
docker-compose up -d
``` 

#### Production (Windows)
```
git clone git@gitlab.com:dcd018/settlement-transfers.git
cd settlement-transfers
docker-compose -f docker-compose.yml -f docker-compose.prod.yml up -d
``` 

### API

#### Routing interface
The LNPP stack uses Laravel versioned API resources with request validation. The container running NGINX will serve the Laravel application over port `8080` from a proxied FastCGI server listening on `9000` running in a separate container.

##### - /api/v1/businessDates/getBusinessDateWithDelay

###### params
- `initialDate`: Required, DateTime string
- `delay`: Required, numeric

###### Example
Accepts parameters as either query string or post body
```
GET:
curl -i -H "Accept: application/json" "http://localhost:8080/api/v1/businessDates/getBusinessDateWithDelay?initialDate=2019-12-12T10:10:10Z&delay=3" 


POST:
curl -d '{"initialDate": "2019-12-12T10:10:10Z", "delay": 3}' -H "Content-Type: application/json" -X POST http://localhost:8080/api/v1/businessDates/getBusinessDateWithDelay
```

#### Pub/Sub interface
The API registers a `BusinessDate` event along with a `BankWire` listener and `BankWire` subscription. When a request is made to `/api/v1/businessDates/getBusinessDateWithDelay`, subscriptions will log the event containing the API Resource in `storage/logs/subscription.log`.

#### Logging interface
Default Monolog facilities are being utilized.