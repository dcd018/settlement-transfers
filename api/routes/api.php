<?php

// v1 API
Route::group(['prefix' => 'v1',], function ($router) {
    require base_path('routes/api.v1.php');
});