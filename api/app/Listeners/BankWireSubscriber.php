<?php

namespace App\Listeners;

use App\Events\BusinessDate;

class BankWireSubscriber
{
    /**
     * Handle business date events.
     */
    public function handleBusinessDate($event) 
    {
        \Log::channel('subscriptions')->debug($event->toString());
    }

    /**
     * Register the listeners for the subscriber.
     *
     * @param  \Illuminate\Events\Dispatcher  $events
     */
    public function subscribe($events)
    {
        $events->listen(
            'App\Events\BusinessDate',
            'App\Listeners\BankWireSubscriber@handleBusinessDate'
        );
    }
}