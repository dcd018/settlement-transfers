<?php

namespace App\Listeners;

use App\Events\BusinessDate;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class BankWire
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BusinessDates  $event
     * @return void
     */
    public function handle(BusinessDate $event)
    {
        //
    }
}
