<?php

namespace App\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class BusinessDate
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $businessDate;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($businessDate)
    {
        $this->businessDate = $businessDate;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }

    public function toString()
    {
        return json_encode($this->businessDate->jsonSerialize());
    }
}
