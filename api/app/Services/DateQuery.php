<?php

namespace App\Services;

use App\Services\BankHolidays;

class DateQuery
{
    public function __construct(BankHolidays $bankHolidays)
    {
        $this->bankHolidays = $bankHolidays;
    }

    public function new($initialDate, $delay)
    {
        $initialTime = strtotime($initialDate);
        $initialInfo = getdate($initialTime);
        $delayTime = $initialTime + ($delay * 86400);

        $businessDate = date(DATE_ATOM, $delayTime);
        $totalDays = ceil(($delayTime - $initialTime) / 86400);
        $holidayDays = 0;
        $weekendDays = 0;

        for ($i = 0; $i < $delay; $i++) {
            $currentTime = $initialTime + ($i * 86400);
            list('wday' => $wday, 'mon' => $month, 'mday' => $day, 'year' => $year) = getdate($currentTime);
            if ($this->bankHolidays->isHoliday($month, $day, $year)) {
                $holidayDays++;
            }
            if ($wday < 1 || $wday > 5) {
                $weekendDays++;
            }
        }

        return [
            'businessDate' => $businessDate,
            'totalDays' => $totalDays,
            'holidayDays' => $holidayDays,
            'weekendDays' => $weekendDays,
        ];
    }
}