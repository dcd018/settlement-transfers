<?php

namespace App\Services;

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

class BankHolidays
{
    public function __construct(Client $client)
    {
        $client = $client;
        $this->crawler = $client->request('GET', 'https://www.interstatecapital.com/us-bank-holidays/');
    }

    public function byYear($year)
    {
        foreach ($this->crawler->filter('.otable > tbody > tr > th') as $node) {
            if (strpos($node->textContent, (string)$year) !== false && $tr = $node->parentNode) {
                return $this->nextDate($tr, $year);
            }
        }
    }

    public function isHoliday($month, $day, $year)
    {
        if ($holidays = $this->byYear($year)) {
            foreach ($holidays as $holiday) {
                $info = getdate($holiday->getTimestamp());
                if ($month == $info['mon'] && $day == $info['mday']) {
                    return true;
                }
            }
        }
        return false;
    }

    private function nextDate($node, $year, $dates = [])
    {
        if (!$node->nextSibling) {
            return $dates;
        }
        $elm = new Crawler($node->nextSibling);
        if ($dateStr = $elm->filter('td')->first()->text()) {
            $dates[] = \DateTime::createFromFormat('M d, D Y H:i:s', sprintf('%s %s 00:00:00', $dateStr, $year));
        }
        return $this->nextDate($node->nextSibling, $year, $dates);
    }
}