<?php

namespace App\Http\Resources\v1;

use Illuminate\Http\Resources\Json\JsonResource;

class BusinessDateWithDelayResource extends JsonResource
{
    protected $dateQuery;

    public function __construct($dateQuery)
    {
        $this->dateQuery = $dateQuery;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ok' => true,
            'initialQuery' => [
                'initialDate' => $request->initialDate,
                'delay' => $request->delay,
            ],
            'results' => $this->dateQuery,
        ];
    }
}
  